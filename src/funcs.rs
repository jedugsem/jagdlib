use crate::types;
use std::io::Read;
use std::io::Write;
#[cfg(target_family = "unix")]
pub fn safe(safe: types::Db, path: &str) -> Result<String, String> {
    let bin_data = bincode::serialize(&safe).unwrap();
    let mut file = std::fs::File::create(path).unwrap();
    file.write_all(&bin_data).expect("error");
    println!("radinsu{:?}", cfg!(feature = "dev"));
    //println!("{}",tes());
    Ok("sjfa".to_string())
}

#[cfg(target_family = "unix")]
pub fn load(path: &str) -> types::Db {
    //let file = File::open("../txt/data_correct").unwrap();
    let file = std::fs::File::open(path).unwrap();
    let mut buf_reader = std::io::BufReader::new(file);
    let mut content: Vec<u8> = Vec::new();
    buf_reader.read_to_end(&mut content).expect("error");
    let kat: types::Db = bincode::deserialize(&content[..]).unwrap();
    kat
}
