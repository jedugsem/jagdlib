use serde::{Deserialize, Serialize};

use rand::prelude::IteratorRandom;
use rand::seq::SliceRandom;
use rand::thread_rng;
//use rand::Rng;

 #[derive(Debug,Clone, PartialEq,Serialize,Deserialize)]
 enum Mode {
    Thema(usize),
    Random,
    Retest,
    Empty,
 }
 impl Default for Mode {
    fn default() -> Self {
       Self::Random
   }
 }

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Default)]
pub struct Db {
    mode: Mode,
    next: Vec<Question>,
    changed : Vec<Question>,
    pub themas: Vec<Thema>,
}
impl Db {
    pub fn fix_index(&mut self) {
        for i in &mut self.themas {
            println!(
                "{:?}",
                i.quest.iter_mut().enumerate().map(|(i, mut q)| {
                    q.num = i;
                })
            );
        }
    }
    pub fn get_test(&self) -> Db {
        let mut rng = thread_rng();
        let mut db = Db::default();
        for i in &self.themas {
            //let results = rand::seq::index::sample(&mut rng, 10, 3).into_vec();
            let vec: Vec<Question> = i.quest.clone().into_iter().choose_multiple(&mut rng, 20);

            db.themas.push(Thema {
                quest: vec,
                ..i.clone()
            })
        }
        db
    }
    pub fn get_theme(&self, i: usize) -> Db {
        let theme = self.themas[i].clone();
        Db {
            themas: vec![theme],
        }
    }
    pub fn filter_false(&self) -> Db {
        Db {
            themas: self.themas.iter().map(|i| i.filter_false()).collect(),
        }
    }
    pub fn filter_edit(&self) -> Db {
        Db {
            themas: self.themas.iter().map(|i| i.filter_edit()).collect(),
        }
    }
    pub fn filter_right(&self) -> Db {
        Db {
            themas: self.themas.iter().map(|i| i.filter_right()).collect(),
        }
    }

    pub fn shuffle_quests(&self) -> Db {
        Db {
            themas: self
                .themas
                .iter()
                .map(|i| {
                    let mut th = i.clone();
                    th.quest.shuffle(&mut thread_rng());
                    th
                })
                .collect(),
        }
    }
}

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Default)]
pub struct Thema {
    pub name: String,
    pub num: usize,
    pub sub: Vec<Thema>,
    pub quest: Vec<Question>,
}

impl Thema {
    pub fn filter_false(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.fals.is_some())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
    pub fn filter_edit(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.edit.is_some())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
    pub fn filter_right(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.fals.is_none())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct Question {
    pub edit: Option<u64>,
    pub fals: Option<u64>,
    pub num: usize,
    pub question: String,
    pub awnsers: Vec<(usize, String, bool)>,
}
impl Default for Question {
    fn default() -> Self {
        Question {
            edit: Some(0),
            fals: Some(0),
            num: 9999,
            question: "randi".to_string(),
            awnsers: vec![],
        }
    }
}
