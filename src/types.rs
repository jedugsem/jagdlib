use rand::seq::SliceRandom;
//use rand::prelude::IteratorRandom;
use rand::thread_rng;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Default, Clone, PartialEq, Deserialize, Debug, Serialize, Eq)]
pub struct MetaQuests {
    pub quests: Vec<usize>,
}

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Eq)]
pub struct MetaDb {
    pub extra_modes: Vec<String>,
    pub thema_names: Vec<String>,
    pub thema_sizes: Vec<usize>,
    pub name: String,
}
impl Default for MetaDb {
    fn default() -> MetaDb {
        MetaDb {
            name: "".to_string(),
            thema_sizes: vec![],
            thema_names: vec![],
            extra_modes: vec![],
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default, Eq)]
pub struct AudioItem {
    pub name: String,
    pub audio: Vec<u8>,
}
impl fmt::Display for AudioItem {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}
pub fn load_audio() -> Vec<AudioItem> {
    let path = std::path::Path::new("/usr/share/jagenlernen/data/audio/");
    let mut temp = vec![];
    if path.is_dir() {
        for entry in std::fs::read_dir(path).unwrap() {
            let path = entry.unwrap().path();
            if path.is_file() {
                //let file = std::fs::File::open(path).unwrap();
                let buf = std::fs::read(&path).unwrap();
                let temp_name = path.file_name().unwrap().to_str().unwrap();
                let name = temp_name[..temp_name.len() - 4].to_string();
                temp.push(AudioItem { audio: buf, name });
            }
        }
    }
    temp
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Eq)]
pub enum Mode {
    Learn(String),
    Edit((usize, usize)),
}
impl Default for Mode {
    fn default() -> Self {
        Self::Learn("Random".to_string())
    }
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Default, Eq)]
pub struct LearnHistory {
    test: bool,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Default, Eq)]
pub struct EditHistory {
    test: bool,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Default, Eq)]
pub struct Db {
    pub meta: MetaDb,
    pub mode: Mode,
    pub i: usize,
    pub next: Vec<Question>,
    pub changed: Vec<Question>,
    pub themas: Vec<Thema>,
}
impl Db {
    pub fn get_metadb(&self) -> Result<MetaDb, String> {
        Ok(self.meta.clone())
    }
    pub fn get_metaquests(&self, mode: Mode) -> Result<MetaQuests, String> {
        match mode {
            Mode::Edit((thema, quest)) => {
                let mut temp: Vec<usize> = vec![];
                if quest == 0 {
                    for i in &self.themas[thema].get_unedited().quest {
                        temp.push(i.num + 1);
                    }
                } else {
                    for i in &self.themas[thema].quest {
                        temp.push(i.num + 1);
                    }
                }
                Ok(MetaQuests { quests: temp })
            }
            _ => Err("metaquest not available".to_string()),
        }
    }
    pub fn get_themas(&self) -> Vec<String> {
        let mut temp = vec![];
        for i in &self.themas {
            temp.push(i.name.clone());
        }
        temp
    }
    pub fn queue(&mut self, quest: Question) {
        self.changed.push(quest);
    }
    pub fn save_to_disk(&self, path: String) -> Result<String, String> {
        crate::funcs::safe(self.clone(), &path)
    }
    pub fn save(&mut self) {
        for i in &self.changed {
            if i.thema < self.themas.len() && i.num < self.themas[i.thema].quest.len() {
                self.themas[i.thema].quest[i.num] = i.clone();
            }
        }
        self.changed = vec![];
    }
    pub fn next_one(&mut self) -> Result<Question, String> {
        if self.i < self.next.len() {
            let temp = self.next[self.i].clone();
            self.i += 1;
            Ok(temp)
        } else {
            Err("End of Questions".to_string())
        }
    }
    pub fn learn_history(&self) -> Result<LearnHistory, String> {
        Ok(LearnHistory::default())
    }
    pub fn edit_history(&self) -> Result<EditHistory, String> {
        Ok(EditHistory::default())
    }
    pub fn set_mode(&mut self, mode: Mode) {
        match mode.clone() {
            Mode::Learn(i) => {
                let mut test = self.meta.thema_names.clone();
                test.extend(self.meta.extra_modes.clone());
                let index = test.iter().position(|r| r == &i).unwrap();
                let mut temp = vec![];

                match index {
                    0..=4 => {
                        temp.extend(self.themas[index].get_edited().get_unawnsered().quest);
                        temp.shuffle(&mut thread_rng());
                    }
                    5 => {
                        for i in &self.themas {
                            temp.extend(i.get_test_sample().quest);
                        }
                    }
                    6 => {
                        temp.extend({
                            let mut temp = vec![];
                            for i in self.themas.iter() {
                                temp.extend(i.get_edited().get_awnsered().quest);
                            }
                            temp
                        });
                        temp.shuffle(&mut thread_rng());
                    }
                    7 => {
                        temp.extend({
                            let mut temp = vec![];
                            for i in self.themas.iter() {
                                temp.extend(i.get_edited().get_unawnsered().quest);
                            }
                            temp
                        });
                        temp.shuffle(&mut thread_rng());
                    }
                    _ => {}
                }
                self.next = temp;
            }
            Mode::Edit((thema, quest)) => {
                println!("quest{}", quest);

                let mut temp: Vec<Question> = vec![];
                temp.extend(self.themas[thema].get_unedited().quest);
                //println!("deleted:::{:?}",squest);
                let squest = self.themas[thema].quest[quest].clone();
                if let Some(ss) = temp.iter().position(|r| r.num == quest) {
                    temp.remove(ss);
                }
                temp.insert(0, squest);
                self.next = temp;
            }
        }
        self.i = 0;
        self.mode = mode;
    }
}

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Default, Eq)]
pub struct Thema {
    pub name: String,
    pub num: usize,
    pub quest: Vec<Question>,
}

impl Thema {
    fn get_test_sample(&self) -> Thema {
        let mut temp: Vec<Question> = vec![];
        temp.extend(self.quest.choose_multiple(&mut thread_rng(), 20).cloned());
        Thema {
            quest: temp,
            ..self.clone()
        }
    }
    fn get_edited(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.edit.is_some())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
    fn get_unedited(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.edit.is_none())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
    fn get_unawnsered(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.fals.is_none())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
    fn get_awnsered(&self) -> Thema {
        let ss: Vec<Question> = self
            .quest
            .clone()
            .into_iter()
            .filter(|voc| voc.fals.is_some())
            .collect();
        Thema {
            quest: ss,
            ..self.clone()
        }
    }
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize, Eq)]
pub struct Question {
    pub edit: Option<u64>,
    pub fals: Option<u64>,
    pub num: usize,
    pub question: String,
    pub awnsers: Vec<(usize, String, bool)>,

    pub thema: usize,
}
impl Default for Question {
    fn default() -> Self {
        Question {
            edit: None,
            fals: None,
            num: 0,
            thema: 0,
            question: "Questions Missing or Complete".to_string(),
            awnsers: vec![],
        }
    }
}
